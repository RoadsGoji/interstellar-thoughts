A little something for [hackfs](https://hackfs.com/).

### To run it:
1. Make sure you have a recent [node](https://nodejs.org/)
2. Get a [web3.storage API key](https://web3.storage/login/) and put it in CONFIGME.json
3. Run serve.sh or serve.bat!
4. Visit localhost with a web3 browser!
