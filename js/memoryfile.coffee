mod_fs = require 'fs'
mod_upkeep = require './upkeep'

# Idea for the future:
#  Revert to unready state with @stored null'd out when the
#  file hasn't been requested in a long time. (e.g. 10 minutes)

@MemoryFile = class
	constructor: (@path,{autoLoad = yes,@saveTimer = 60000} = {}) ->
		@ready = no
		@broken = no
		@existsOnDisk = undefined
		@load() if autoLoad
	load: =>
		return @stored if @ready
		# fulfils a promise with a string,
		#  or null if the file didn't exist
		#  or rejects on other errors.
		return @primer or (@primer = new Promise (ok,nope) =>
			mod_fs.readFile @path,(problem,stored) =>
				#console.info "yay finished loading",@path
				switch problem and problem.code
					when null
						@existsOnDisk = yes
						@stored = String stored
					when 'ENOENT'
						@existsOnDisk = no
						@stored = ''
					else
						console.error "File access problems",problem
						@broken = yes
						nope problem
						return
				@ready = yes
				ok @stored
		)
	populate: (withString) ->
		throw new Error "Tried to populate an already-loaded file." if @primer
		throw new TypeError if typeof withString isnt 'string'
		#
		#@existsOnDisk = ... # this should stay undefined, since we don't know.
		@stored = withString
		@ready = yes
		@primer = new Promise (ok) -> ok withString
	store: (str) =>
		throw new Error "Store before load." if not @ready
		throw new TypeError if typeof str isnt 'string'
		#
		@stored = str
		@existsOnDisk = yes # think positive
		return if @saveTimerJob
		@saveTimerJob = mod_upkeep.rushableTimer @saveTimer,=>
			@saveTimerJob = null
			await @save()
	###
	# Alternate approach:
	store: (str) =>
		throw new Error "Store before load." if not @ready
		throw new TypeError if typeof str isnt 'string'
		#
		@saveTimerJob.cancel() if @saveTimerJob
		@stored = str
		@saveTimerJob = mod_upkeep.rushableTimer @saveTimer,=>
			@saveTimerJob = null
			await @save()
	###
	save: =>
		await @primer if @primer
		await @saver if @saver # wait for the previous write to finish
		throw new Error("Blocked attempt to save unprepared MemoryFile.") if not @ready
		@existsOnDisk = yes
		return @saver = new Promise (ok,nope) =>
			mod_fs.writeFile @path,@stored,(er) =>
				@saver = null
				if er
					console.error "BIG PROBLEM: Writing error",er
					nope(er)
					#throw new Error "Saving is Broken: #{@path}"
					return
				console.log 'Saved:',[@path]
				ok()
