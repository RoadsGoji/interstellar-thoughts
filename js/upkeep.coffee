###

I find something like these things useful on most server projects.

@monitorFiles monitors the filesystem for changes to key
 files and quits the server with exit code 99 if it detects
 a change.
The accompanying serve.sh/serve.bat starts the server again
 immediately if it sees code 99.

The other part of this is a Ctrl-C catcher to avoid cmdline
 accidents, and to let pending writes finish if a shutdown
 is needed.
If the server uses websockets it's also an opportunity to tell
 every client the server is going down for a while.

###

mod_fs = require 'fs'


uid = 90
stallers = {}
rushables = []
@waitForMe = waitForMe = (staller) ->
	# call this when you have something that
	#  would like to stall shutdown.
	throw new TypeError if typeof staller.hurryUp isnt 'function'
	nuid = uid++
	stallers[nuid] = staller
	return -> delete stallers[nuid]
@rushableTimer = (delay,finalise) ->
	alldone = null
	rushed = no
	cancelled = no
	timer = null
	alldone = waitForMe
		hurryUp: ->
			return if cancelled
			rushed = yes
			clearTimeout timer
			await finalise()
	timer = setTimeout (->
		return if rushed or cancelled
		alldone()
		await finalise()
	),delay
	return
		cancel: ->
			cancelled = yes
			clearTimeout timer
			alldone()
hurryUp = ->
	# freeze the keys before looping.
	waiters = []
	keys = Object.keys stallers
	for uid from keys
		waiters.push stallers[uid].hurryUp()
	try
		if waiters.length
			console.log "Postponing shutdown for #{ waiters.length } task#{ if waiters.length is 1 then '' else 's' }."
		await Promise.all waiters
	catch ee
		console.error "SOMETHING didn't go right during hurryUp().",ee

@monitorFiles = ->
	wayout = no
	mod_fs.watch '.',(tx,fn) ->
		return if wayout
		if tx is 'change'
			console.log """Root file changed: "#{tx}" / "#{fn}" """
			if (
				# Restart server when any of these files changes:
				(fn.match /\bapp\.js$/i) or
				(fn.match /\bconfigme\.json$/i) or
				(fn.match /\bbump\.uid$/i)
			)
				wayout = yes
				console.log '-- Reboot --'
				#!!#md_save()
				safe_shutdown()
	mod_fs.watch './js/',(tx,fn) ->
		return if wayout
		if tx is 'change'
			console.log """Module file changed: "#{tx}" / "#{fn}" """
			if (
				(fn.match /\.js$/i)
			)
				wayout = yes
				console.log '-- Reboot --'
				#!!#md_save()
				safe_shutdown()
	try mod_fs.watch './themes/',(recursive:yes),(tx,fn) ->
		return if wayout
		if tx is 'change'
			console.log """Theme file changed: "#{tx}" / "#{fn}" """
			if (
				(fn.match /\.html$/i) or
				(fn.match /\.css$/i)
			)
				wayout = yes
				console.log '-- Reboot --'
				safe_shutdown()

seriously = 0
@catchCtrlC = ->
	process.on 'SIGINT',->
		seriously += 1
		switch seriously
			when 1 then console.log("Ctrl-C absorbed. 2 more to quit.")
			when 2 then console.log("Ctrl-C absorbed. 1 more to quit.")
			when 4 then console.warn("Ctrl-C absorbed. Next will skip safe-shutdown and terminate.")
			when 5
				console.warn("Shutting down UNSAFELY.")
				process.exit 92
			else
				safe_shutdown()

safe_shutdown = ->
	console.log "Beginning safe shutdown."
	await hurryUp()
	final_shutdown()

final_shutdown = ->
	console.log "Shutting down."
	setTimeout(->
		if seriously >= 3 
			process.exit 90
		else
			process.exit 99
	,125)
