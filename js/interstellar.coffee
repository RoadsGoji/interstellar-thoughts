###

Interstellar logs are the second-most important
 files among Interstellar servers. They point
 to CIDs of posted-thoughts; they're used for
 profile feeds, home indices, and most importantly:
 for easy user migration between servers.

###


# These get awkwardly overridden by CONFIGME.json
archive_this_many_posts = 32
max_posts_before_archival = 2 * archive_this_many_posts
homepage_post_limit = 64


# Modules
mod_w3s = require 'web3.storage'
mod_memfile = require './memoryfile'


# Should not be here
w3s = null
@smuggle = (sneak) ->
	{w3s,archive_this_many_posts,homepage_post_limit} = sneak
	max_posts_before_archival = 2 * archive_this_many_posts


# Actual
Log = class
	constructor: (@name) ->
		throw new Error("Internal sanity check on #{@name} - it should have no ext and no path.") if /[\.\/]/.test @name
		@path = "db/#{@name}.sw"
		@file = new mod_memfile.MemoryFile @path
		@ready = no
		@getReady()
	getReady: =>
		@primer = @file.load()
		@lines = (await @primer)
			.split '\n'
			.map (ln) => ln.trim()
			.filter (Boolean)
		@ready = yes
	unshift: (ln) =>
		await @primer
		#console.log 'Emergency',@ready,@primer,@lines
		await @activeWrite if @activeWrite
		return @activeWrite = new Promise (ok,nope) =>
			@lines.unshift ln
			switch @indexMode
				when 'HOMEPAGE'
					await @maybeTrim(homepage_post_limit)
				when undefined
					await @maybeArchive()
				else
					throw new Error "Unknown indexMode on #{ @name }"
			@save() # we can't wait for this because it's a background job.
			ok()
	discard: (limit,how) => # the limit is so that a miscall doesn't delete the whole log!
		throw new TypeError if typeof limit isnt 'number'
		throw new TypeError if typeof how isnt 'function'
		#
		await @primer
		changed = no
		ii = -1
		while ++ii < @lines.length
			break if limit is 0
			if how @lines[ii]
				@lines.splice ii,1
				changed = yes
				ii -= 1
				limit -= 1
		@save() if changed
	readFirst: (matcher) ->
		throw new TypeError if not (matcher instanceof RegExp)
		await @primer
		for ln from @lines
			return ln if matcher.test ln
		return null
	save: =>
		@file.store @lines.join '\n'
	asText: =>
		await @primer
		return @file.stored
	maybeTrim: (limit) =>
		counted = 0
		ii = -1
		do_trim = no
		while ++ii < @lines.length
			line = @lines[ii]
			if /^\/(?:post) /.test line
				counted += 1
				if counted >= limit
					@lines.splice ii,1
					ii -= 1
		return yes
	maybeArchive: =>
		# checking these every time is inefficient but fine for now
		counted = 0
		do_archive = no
		ii = -1
		while ++ii < @lines.length
			line = @lines[ii]
			if /^\/(?:post|archive) /.test line
				counted += 1
				break if do_archive = (counted >= max_posts_before_archival)
		return if not do_archive
		#
		# Start from the end and walk back:
		ii = @lines.length
		archive = []
		replacement_lines = @lines.slice(0)
		while --ii >= 0 # should be impossible to hit 0, but some paranoia's nice.
			line = @lines[ii]
			if /^\/(?:post|archive) /.test line
				archive.unshift line
				replacement_lines.splice ii,1 # don't do this on @lines directly in case the w3s upload fails
				break if archive.length >= archive_this_many_posts
		archive_ready = archive.join '\n'
		#
		arch_file = new mod_w3s.File [archive_ready],"archive.sw"
		try
			await w3s.put [arch_file],
				name: "Archive of #{@name}"
				onRootCidReady: (cid) ->
					replacement_lines.push "/archive #{ cid }"
				#onStoredChunk: ->
			# success!
			@lines = replacement_lines
			return yes
		catch ee
			console.warn "Archive-Upload likely didn't happen. Data is safe, but server is storing more than chosen limits. Better luck next time.",ee
			return no


# The one we actually wanna use
created = no
@LogWall = new class
	constructor: ->
		throw new Error "LogWall is a singleton." if created
		created = yes
		#super()
		@wall = {}
	fromDB: (name,indexMode) =>
		throw new Error("Primary sanity check on #{fn} - it should have no ext and no path.") if /[\.\/]/.test name
		if not (hit = @wall[name])
			hit = @wall[name] = new Log name
		if indexMode
			if typeof hit.indexMode is 'undefined'
				hit.indexMode = indexMode
			else
				if hit.indexMode != indexMode
					console.error hit.indexMode,"!=",indexMode,"- we're gonna crash."
					throw new Error "fromDB() called on #{ name } with DIFFERENT index modes."
		return hit