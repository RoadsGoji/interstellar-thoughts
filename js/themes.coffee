mod_fs = require 'fs'

@readThemeSync = (theme) ->
	throw new Error("Theme name sanity check.") if /\.\//.test theme
	#
	snipRead = (snip,ext = 'html') ->
		try
			return String mod_fs.readFileSync("themes/#{theme}/#{snip}.#{ext}")
		catch ee
			if ee.code is 'ENOENT'
				return String mod_fs.readFileSync("themes/fallback/#{snip}.#{ext}")
			throw ee
	#
	return
		"sys-prelude":  snipRead 'sys-prelude'
		"sys-gfx":      snipRead 'sys-gfx'
		"sys-style":    snipRead 'sys-style'
		"sys-header":   snipRead 'sys-header'
		"page-home":    snipRead 'page-home'
		"page-profile": snipRead 'page-profile'
