
#
# Node modules
mod_w3 = require 'web3'
mod_w3s = require 'web3.storage'
mod_fs = require 'fs'
mod_os = require 'os'
mod_path = require 'path'
mod_http = require 'http'




#
# Local Modules
mod_themes = require './js/themes.js'
mod_upkeep = require './js/upkeep.js'
mod_interstellar = require './js/interstellar.js'



#
# Flags that should move to proper-config
max_posts_before_archival = 64
#max_age_before_archival # not implemented, but a good idea.
max_lines_on_migration = (max_posts_before_archival * 4) + 24



#
# Config & Setup
if not mod_fs.existsSync "./db"
	mod_fs.mkdirSync "./db"

configActual = JSON.parse(String(mod_fs.readFileSync("CONFIGME.json")));

# web3storage:
if not configActual["web3storage-api-key"]
	#throw new Error "Drop your web3.storage API key into CONFIGME.json to get started."
	console.log '\n~~~ Drop your web3.storage API key into CONFIGME.json to get started! ~~~\n'
	process.exit 9
w3s = new mod_w3s.Web3Storage token:configActual["web3storage-api-key"]
mod_interstellar.smuggle {
	w3s
	archive_this_many_posts: configActual['archive-page-size'] or 32
	homepage_post_limit: configActual['homepage-post-limit'] or 64
}
max_post_clock_drift = 1000 * 60 * 10 # 10 minutes of clock drift allowed for posts, back or forward

# web3:
web3 = new mod_w3

# theme:
themeActual = mod_themes.readThemeSync configActual['theme']

# server restart and ctrl-c catching stuff:
queueMicrotask mod_upkeep.monitorFiles
queueMicrotask mod_upkeep.catchCtrlC




#
# Helpers
OK =
	attr: (ss) -> String(ss).replace(/&/g, '&#x26;').replace(/'/g, '&#x27;').replace(/"/g, '&#x22;')
	html: (ss) -> String(ss).replace(/&/g, '&#x26;').replace(/</g, '&#x3C;')
ender = (res,code,more) ->
	res.writeHead code,
		'Content-Type':'text/plain'
		'Access-Control-Allow-Origin': '*'
	res.write "#{code}\n"
	res.write "\n#{more}\n" if more
	res.end()



#
# Welcome
console.log ["         -= Interstellar Thought Server on " + mod_os.hostname() + " =-         "]



#
# Tests
testmode = no
if testmode then do ->
	#mod_memfile = require './js/memoryfile';
	#honk = new mod_memfile.MemoryFile './testtt2.txt'
	#await honk.load()
	#honk.store '1992'
	waller = mod_interstellar.LogWall.fromDB 'testtt-99'
	await waller.unshift 'Honk'
	console.log 'aha!',await waller.asText()


#
# PUT helper
intakeRequest = (soft_limit,req) ->
	throw new TypeError if typeof soft_limit isnt 'number'
	hard_limit = soft_limit * 0x10 # limit after which the requestor is forcefully disconnected
	return new Promise (ok,nope) ->
		total_intake = 0
		gather = []
		bad = no
		req.on 'data',(intake) ->
			if bad
				if total_intake > hard_limit
					req.connection.destroy()
				return
			gather.push String(intake)
			total_intake += intake.length
			if total_intake > soft_limit
				bad = yes
		#req.pipe ws
		req.on 'end',() ->
			if bad
				#nope() # would prefer this promise not reject
				ok null
			else
				ok gather.join ''

#
# Actual
mod_http.createServer hanandle = (req, res) ->
	return ender res,400,'..' if req.url.match /\.\./ # no dot-dot requests, anywhere
	rp = decodeURI req.url
	hst = (req.headers.host or '???').split(':')[0]
	#
	rp = rp.split('?')[0]
	rp = "#{rp}index.html" if rp.match /\/$/
	#
	pth = rp.split '/'
	pth = pth.map (ss) -> ss.toLowerCase()
	console.log req.connection.remoteAddress,"wants",pth # <- a thought: could somebody screw around with this by injecting terminal escape codes?
	return ender res,400,"Peculiar path." if pth[0]
	#
	if req.method is 'PUT' and (match = rp.match /^\/post\/0x([0-9A-Fa-f]{40})\/0x([0-9A-Fa-f]{130})$/)
		#
		[_,addr,siggy] = match
		#
		intakeRequest(0x400,req).then (bdy) ->
			if typeof bdy isnt 'string' # means error
				return ender res,413,"Too much data." #"tl;dw"
			#
			console.log "Happily received #{addr}, and",bdy
			#
			# Parse opening lines for @ and RE: headers
			# See also: 1FE75924-9C2E-442E-A9C6-51A89FAE6F61
			lines = bdy.split('\n',9) # not expecting many headers in the beginning but I'd rather be generous
			ii = -1
			ts = null
			while ln = lines[++ii] # break on first blank line or at EOM
				if /^@\d+$/.test ln
					# it's a timestamp
					ts = new Date Number ln.substr(1)
			#
			now = +new Date
			if Math.abs(now - ts) > max_post_clock_drift
				# this is a good enough replay-attack-preventer for on-planet posts, but it needs work.
				return ender res,400,"Your device's clock-time and the server's are not close enough to each other."
			#
			try
				signer = web3.eth.accounts.recover(bdy,"0x"+siggy);
			catch ee
				console.warn "web3 hates it:",siggy
				return ender res,400,"Dubious signature error."
			console.log signer,"vs.",addr
			if signer.toLowerCase() isnt "0x"+addr.toLowerCase()
				console.log "IMPOSTOR"
				return ender res,401,"Sloppy signature error."
			#else
			console.log "All Clear"
			#
			thought_file = new mod_w3s.File [bdy],"thought.txt"
			siggy_file = new mod_w3s.File [JSON.stringify eth:addr,sig:siggy],"verify.json"
			console.log "Here it goes.."
			said = no
			try
				hold_it = []
				home_log = mod_interstellar.LogWall.fromDB "index-home",'HOMEPAGE'
				personal_log = mod_interstellar.LogWall.fromDB "addr-#{addr}"
				console.log 'Trying readFirst()'
				display_name = ((await personal_log.readFirst /^\/name /) or '/name ').substr('/name '.length)
				await w3s.put [thought_file,siggy_file],
					name: "Thought from #{addr}"
					onRootCidReady: (cid) ->
						# First, check if this CID is in the log already; if it is, it's a double-post. We'll assume it's not malicious and take no action.
						reg = new RegExp("\\b#{ cid }\\b") # we could be REALLY paranoid and check that the CID is valid first, but I have faith.
						if await personal_log.readFirst reg
							# already posted. ntdh.
							console.log "Ignoring double-post or replay attack:",reg
							return
						hold_it.push personal_log.unshift "/post ~#{addr} #{cid} @#{ +new Date }"
						hold_it.push home_log.unshift "/post ~#{addr} #{cid} @#{ +new Date }#{ if display_name then " :#{display_name}" else "" }"
					#onStoredChunk: ->
				console.log "Main upload worked; waiting on any saves/archives."
				await Promise.all hold_it
				ender res,204,""
			catch ee
				console.warn "Upload likely didn't happen; post was likely logged prematurely.",ee
				ender res,502,"Upload was not possible."
		return
	else if req.method is 'PUT' and (match = rp.match /^\/repost\/0x([0-9A-Fa-f]{40})\/0x([0-9A-Fa-f]{130})$/)
		#
		[_,addr,siggy] = match
		#
		intakeRequest(0x400,req).then (bdy) ->
			if typeof bdy isnt 'string' # means error
				return ender res,413,"Too much data." #"tl;dw"
			#
			try
				{eth,cid,link} = JSON.parse bdy
				throw new SyntaxError if not /^[a-z]{2,6}:\/\/[^\0\cA-\cZ]+\/$/.test link
			catch ee
				console.warn "Rejecting repost.",ee
				return ender res,400,"Bad data."
			#
			console.log "Happily re-received #{addr}, and",eth,cid,link
			#
			# TODO: VERIFY that this is a real Interstellar IPFS post and not some junk
			# TODO: VERIFY that this is a real Interstellar IPFS post and not some junk
			# TODO: VERIFY that this is a real Interstellar IPFS post and not some junk
			# TODO: VERIFY that this is a real Interstellar IPFS post and not some junk
			#
			try
				signer = web3.eth.accounts.recover("Repost from #{ link }:\n\n#{ cid }","0x"+siggy);
			catch ee
				console.warn "web3 hates it:",siggy
				return ender res,400,"Dubious signature error."
			console.log signer,"vs.",addr
			if signer.toLowerCase() isnt "0x"+addr.toLowerCase()
				console.log "IMPOSTOR"
				return ender res,401,"Sloppy signature error."
			#else
			console.log "All Clear"
			#
			hold_it = []
			personal_log = mod_interstellar.LogWall.fromDB "addr-#{addr}"
			await personal_log.unshift "/post ~#{eth} #{cid} @#{ +new Date } #{ link }"
			home_log = mod_interstellar.LogWall.fromDB "index-home",'HOMEPAGE'
			await home_log.unshift "/post ~#{eth} #{cid} @#{ +new Date } #{ link }"
			ender res,204,""
		return
	else if req.method is 'PUT' and (match = rp.match /^\/profile\/0x([0-9A-Fa-f]{40})\/?$/)
		return ender res,501,"It's not ready."
		intakeRequest(0x100,req).then (bdy) ->
			if typeof bdy isnt 'string' # means error
				return ender res,413,"Too much data." #"tl;dw"
			#
			try
				jsn = JSON.parse bdy
			catch
				return ender res,400,"Malformed upload." #"tl;dw"
			#
			# TODO: ACTUALLY VERIFY that the owner sent this!
			# TODO: ACTUALLY VERIFY that the owner sent this!
			# TODO: ACTUALLY VERIFY that the owner sent this!
			# TODO: ACTUALLY VERIFY that the owner sent this!
			#
			personal_db = mod_interstellar.LogWall.fromDB("addr-#{addr}")
			if jsn.name
				if jsn.name.match /[\0\cA-\cZ]/ # I feel I'll create a legacy of madness if I don't also exclude " & and <
					return ender res,400,"Highly dubious name alert."
				if jsn.name.length > 128 # this is VERY generous
					return ender res,400,"Your name is rather long."
				personal_db.discard 1,(line) -> /^\/name /.test line
				personal_db.unshift "/name #{jsn.name}"

	#
	# GETs
	else if pth[1] is ".well-known" # add more of these if you want traditional file passthrough serving
		mod_fs.readFile rp,(er,cn) ->
			if er then return switch er.code
				when 'ENOENT'
					console.log req.connection.remoteAddress,"404'd",rp
					ender res,404
				else
					console.log req.connection.remoteAddress,"500'd",rp
					ender res,500,er.code
			res.writeHead 200,
				'Content-Type': 'application/octet-stream'
				'Cache-Control':"max-age=#{ 0 }"
			res.write cn
			res.end()
		return
	else if pth.length is 3 and pth[1] is 'strange-writing'
		try
			# this will throw if it doesn't like the filename:
			log = mod_interstellar.LogWall.fromDB pth[2]
			log.asText().then (stored) ->
				res.writeHead 200,
					'Content-Type': 'text/plain'
					'Cache-Control':"max-age=#{ 0 }"
					'Access-Control-Allow-Origin': '*'
				res.write stored
				res.end()
			return
		catch ee
			console.log "/strange-writing/ request subverted by",ee
			return ender res,400,"Unwelcome filename."
	else if pth.length is 2 and pth[1] is  'index.html'
		log = mod_interstellar.LogWall.fromDB "index-home",'HOMEPAGE'
		res.writeHead 200,
			'Content-Type': 'text/html'
			'Cache-Control':"max-age=#{ 0 }"
			'Access-Control-Allow-Origin': '*'
		try
			log.asText().then (send) ->
				#
				# Overzealously disallow characters that could cause code injection
				if /[`\\<"&]/.test send
					console.warn "Skipped sending contents of MAIN INDEX through text/html due to possible injection."
					send = ''
				#
				res.write [
					themeActual['sys-prelude']
					themeActual['sys-gfx']
					themeActual['sys-style']
					themeActual['sys-header'].replace /ACF98A03-C0AD-4498-98B9-871F88BE477B/g, hst
					themeActual['page-home'].replace /ADF26198-411C-414A-9EB3-2C981305110E/g, send
				].join '\n'
				res.end()
		return
	#
	# Redirect /0xADDRESS and /0xADDRESS/ to /ADDRESS/
	else if (pth.length is 2 or pth.length is 3) and /^0x[a-z0-9]{40}$/.test pth[1]
		res.writeHead 301,
			'Location':"/#{ pth[1].substr(2) }/"
		res.end()
		return
	#
	# Redirect /ADDRESS to /ADDRESS/
	else if pth.length is 2 and /^[a-z0-9]{40}$/.test pth[1]
		res.writeHead 301,
			'Location':"/#{ pth[1] }/"
		res.end()
		return
	else if pth.length is 3 and /^[a-z0-9]{40}$/.test pth[1]
		log = mod_interstellar.LogWall.fromDB "addr-#{pth[1]}"
		try
			log.asText().then (send) ->
				if not log.file.existsOnDisk
					return ender res,404,"No such number."
				#
				# Overzealously disallow characters that could cause code injection
				if /[`\\<"&]/.test send
					console.warn "Skipped sending contents of",pth[1],"through text/html due to possible injection."
					return ender res,503,"Apparent data corruption."
				#
				# Ready to send:
				res.writeHead 200,
					'Content-Type': 'text/html'
					'Cache-Control':"max-age=#{ 0 }"
					'Access-Control-Allow-Origin': '*'
				res.write [
					themeActual['sys-prelude']
					themeActual['sys-gfx']
					themeActual['sys-style']
					themeActual['sys-header'].replace /ACF98A03-C0AD-4498-98B9-871F88BE477B/g, hst
					themeActual['page-profile']
						.replace /ADF26198-411C-414A-9EB3-2C981305110E/g, send
						.replace /510FC321-DD3F-4495-BD43-7C97AABC3857/g, pth[1]
				].join '\n'
				res.end()
		catch ee
			console.error "Possible file access problems",er
			return ender res,500,"Something is not right."
		return
	#else
	#
	return ender res,400,"Not available."
.listen configActual["www-port"]
